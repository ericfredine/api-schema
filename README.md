# api-schema

## Principles

- The schema represents a super set of all possible fields and relationships.
- Each integration will have a ERP specific validation rules. These rules will typically be provided when the legal entity is set up for a particular ERP.
- List items will be represented be represented as distinct types. That is, rather than Vendor being a list of type Vendor will be a first class type.
- The schema defines all possible associations. The validation rules define associations applicable to a particular ERP. That is, if an Item can only appear on an Invoice then it would only be defined as a valid field on an Invoice! Similarly, if there are associations between Vendor and Account this would appear as a valid relationship in the schema.

## Conventions

In the json examples, CamelCase is used for references such as VendorRef. I think these would likely be references based on the ERP unique key for the item. We would also have our own Beanworks id for each item.

## List Items

All predefined list items in Beanworks will have their own type. They may implement a common List Interface (in GraphQL terms). There will also be a custom list type which allows integration specific lists to be defined.

### List Item Schemas

List items will need a schema for each list type and we will need to validate the incoming data against this schema. We don't really do this level of validation today - we implicitly assume the data is in the same format.

There are plenty of list specific attributes. So, it's likely that many list type definition will have specific fields. For each list type, different ERP will need to be a super-set of all the fields we support.

There are definitely some ERP specific behaviors baked into our software for ERP like Sage100 and Sage300 where we have gone 'further down the mineshaft'.

I've sort of glossed over the concept of associations, but this is a deep topic. Also, topics like unit of measure also tend to be pretty deep.

Some example json records for different list types:
[./ListItems.json](./ListItems.json)

We have many different list types. The API will need to define schemas for all of them. Fortunately, at least some of them won't have too many list specific fields.

### List Configuration

The display of lists is configurable using Moustache templates. I imagine this functionality being exposed through an API. When the ERP is configured, the display templates would be defined.

I don't think we need to expose the idea of a structure template through the API.

## Importing Payments to BWAP

There are a couple of things we need to do/constrain when it comes to payments:

- we need to be able to match each payment to the invoices it is paying. The easiest way to do this from an API point of view is to require the payments to reference the invoice by its Beanworks id;
- we also need to NOT re-import payments that were exported by Beanworks.

The simplest solution to both of these is to make them the responsibility of the integrator. That's probably the right thing to do.

A sort of minimalist version of a payment:
[./Payment.json](./Payment.json)

Note that references to types such as VendorRef would be marked as either optional or required and would be a reference to a particular type of entity such as a vendor.

## Purchase Order Adoption Models

I'd say that we effectively support three models for adopting from purchase orders:

1. Invoice lines adopt from the purchase order as a whole ignoring the purchase order lines -- this is the way the `Beanworks Purchase Orders` work.
2. Invoice lines adopt from purchase order lines: this is how `Sage100` works; and
3. Invoice lines adopt from receiving lines.

## Importing Purchase Orders and Receivings

Purchase orders and receivings are typically linked. The structure of a purchase order might look something like:
[./PurchaseOrder.json](./PurchaseOrder.json)

Taxes is a kind of a deep topic.

They are probably many other possible fields.

## Exporting Invoices and Payments from BWAP

The fields present in an invoice will vary based on whether or not it's adopted from a PO. An example based on our current Sage100 model might look something like this:
[./Invoice.json](./Invoice.json)
